package com.ssc.entity;

import java.util.Collections;
import java.util.List;

public class Deck {
    private final List<Card> cards;
    private int topCard;

    public Deck(List<Card> cards) {
        this.cards = cards;
    }

    public Card getNextCard() {
        return cards.get(topCard++);
    }

    public void shuffle() {
        Collections.shuffle(this.cards);
    }
}
