package com.ssc.entity;

import com.ssc.action.Decision;
import com.ssc.action.DecisionFactory;
import com.ssc.game.BlackJack;
import com.ssc.util.ActionEnum;
import com.ssc.util.DecisionEnum;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Dealer extends Player {

    private final BlackJack game;

    public Dealer(String name, BlackJack game) {
        super(name);
        this.game = game;
    }

    @Override
    public Decision getDecision(Hand hand, List<DecisionEnum> availableActions, DecisionFactory decisionFactory) throws IOException {
        int highestScore = getHighestScore();
        if (hand.getHandValue() > highestScore || hand.getHandValue() >= 18) {
            return decisionFactory.make(DecisionEnum.STAND);
        }
        return decisionFactory.make(DecisionEnum.HIT);
    }

    @Override
    public boolean isDealer() {
        return true;
    }

    @Override
    public List<DecisionEnum> getAvailableActions(Hand hand) {
        List<DecisionEnum> availableDecisions = new ArrayList<>();
        availableDecisions.add(DecisionEnum.HIT);
        availableDecisions.add(DecisionEnum.STAND);
        return availableDecisions;
    }

    private int getHighestScore() {
        int highestScore = 0;
        for (Player player : this.game.getPlayers()) {
            if (!player.isDealer()) {
                for (Hand hand : player.getHands()) {
                    if (hand.getState() == ActionEnum.STAND || hand.getState() == ActionEnum.NONE) {
                        if (hand.getHandValue() > highestScore) {
                            highestScore = hand.getHandValue();
                        }
                    }
                }
            }
        }
        return highestScore;
    }

}
