package com.ssc.entity;

import com.ssc.action.Decision;
import com.ssc.action.DecisionFactory;
import com.ssc.game.BlackJack;
import com.ssc.util.DecisionEnum;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public abstract class Player {
    private String name;
    private List<Hand> hands = new LinkedList<>();
    private int balance;

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public List<Hand> getHands() {
        return this.hands;
    }

    public void emptyHands() {
        this.hands = new LinkedList<>();
    }

    public int getBalance() {
        return this.balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void addCard(Card card, Hand hand) {
        hand.addCard(card);
    }

    public void addHand(Hand hand) {
        this.hands.add(hand);
    }

    public Hand createFirstHand() {
        if (!BlackJack.isGameStarted) {
            this.hands.add(new Hand());
            return this.hands.get(0);
        }
        throw new IllegalStateException("Can not create hand.");
    }

    public abstract Decision getDecision(Hand hand, List<DecisionEnum> availableActions, DecisionFactory decisionFactory) throws IOException;

    public abstract boolean isDealer();

    public abstract List<DecisionEnum> getAvailableActions(Hand hand);
}
