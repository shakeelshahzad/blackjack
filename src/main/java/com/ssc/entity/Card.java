package com.ssc.entity;

public class Card implements Cloneable {
    private Suite suite;
    private Rank rank;

    public Card(Suite suite, Rank rank) {
        this.suite = suite;
        this.rank = rank;
    }

    public Rank getRank() {
        return this.rank;
    }

    public Suite getSuite() {
        return this.suite;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(rank.face);
        sb.append(suite.suite);
        return sb.toString();
    }

    @Override
    protected Object clone() {
        Card clone = null;
        try {
            clone = (Card) super.clone();
            clone.suite = this.suite;
            clone.rank = this.rank;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        return clone;
    }

    public enum Suite {
        SPADES('♠'),
        CLUBS('♣'),
        DIAMONDS('♦'),
        HEARTS('♥');

        private char suite;

        Suite(char suite) {
            this.suite = suite;
        }
    }

    public enum Rank {
        ACE("A", 1),
        TWO("2", 2),
        THREE("3", 3),
        FOUR("4", 4),
        FIVE("5", 5),
        SIX("6", 6),
        SEVEN("7", 7),
        EIGHT("8", 8),
        NINE("9", 9),
        TEN("10", 10),
        JACK("J", 10),
        QUEEN("Q", 10),
        KING("K", 10);

        private String face;
        private int faceValue;

        Rank(String face, int faceValue) {
            this.face = face;
            this.faceValue = faceValue;
        }

        public int getFaceValue() {
            return this.faceValue;
        }
    }
}
