package com.ssc.entity;

import com.ssc.action.Decision;
import com.ssc.action.DecisionFactory;
import com.ssc.util.DecisionEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Human extends Player {

    private static final Logger logger = LogManager.getLogger(Human.class);
    final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final String CHOOSE_VALID = "Please make choice from available actions.";

    public Human(String name) {
        super(name);
    }

    @Override
    public Decision getDecision(Hand hand, List<DecisionEnum> availableActions, DecisionFactory decisionFactory) throws IOException {
        while (true) {
            try {
                int decision = Integer.parseInt(reader.readLine());
                DecisionEnum chosen = DecisionEnum.getDecision(decision - 1);
                for (DecisionEnum d : availableActions) {
                    if (chosen == d) {
                        return decisionFactory.make(chosen);
                    }
                }
                logger.error(CHOOSE_VALID);
            } catch (IllegalStateException e) {
                logger.error(CHOOSE_VALID); // Digesting exception on purpose
            } catch (NumberFormatException n) {
                logger.error(CHOOSE_VALID); // Digesting exception on purpose
            }
        }
    }

    @Override
    public boolean isDealer() {
        return false;
    }

    @Override
    public List<DecisionEnum> getAvailableActions(Hand hand) {
        List<DecisionEnum> availableDecisions = new ArrayList<>();

        if (hand.canHit()) {
            availableDecisions.add(DecisionEnum.HIT);
        }
        if (hand.canStand()) {
            availableDecisions.add(DecisionEnum.STAND);
        }
        if (hand.canDoubleDown()) {
            availableDecisions.add(DecisionEnum.DOUBLE_DOWN);
        }
        if (hand.canSplit()) {
            availableDecisions.add(DecisionEnum.SPLIT);
        }
        if (isFirstRound() && hand.canSurrender()) {
            availableDecisions.add(DecisionEnum.SURRENDER);
        }
        return availableDecisions;
    }

    public boolean wantsToPlay() throws IOException {
        return !(reader.readLine().equalsIgnoreCase("q"));
    }

    public void placeBet(int amount, Hand hand) {
        hand.addBet(amount);
        this.setBalance(this.getBalance() - amount);
    }

    // PRIVATE BELOW HERE

    private boolean isFirstRound() {
        return (this.getHands().size() == 1);
    }
}
