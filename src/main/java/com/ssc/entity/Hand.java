package com.ssc.entity;

import com.ssc.game.BlackJack;
import com.ssc.util.ActionEnum;

import java.util.LinkedList;
import java.util.List;

public class Hand {

    private final List<Card> cards = new LinkedList<>();
    private int bet;
    private ActionEnum state = ActionEnum.NONE;

    public void addCard(Card card) {
        this.cards.add(card);
    }

    public List<Card> getCards() {
        return this.cards;
    }

    public int getHandValue() {
        int numOfAces = 0;
        int value = 0;
        for (Card card : this.cards) {
            if (card.getRank() == Card.Rank.ACE) {
                numOfAces++;
                continue;
            }
            value += card.getRank().getFaceValue();
        }

        for (int i = 0; i < numOfAces; i++) {
            if (value + 11 > BlackJack.BLACK_JACK) {
                value += Card.Rank.ACE.getFaceValue();
            } else {
                value += 11;
            }
        }
        return value;
    }

    public void addBet(int betValue) {
        this.bet += betValue;
    }

    public int getBetValue() {
        return this.bet;
    }

    public boolean canStand() {
        return (this.getHandValue() < BlackJack.BLACK_JACK);
    }

    public boolean canHit() {
        return (this.getHandValue() < BlackJack.BLACK_JACK);
    }

    public boolean canDoubleDown() {
        int value = this.getHandValue();
        return (value >= 9 && value <= 11 && this.cards.size() == 2);
    }

    public boolean canSplit() {
        return (this.cards.size() == 2 && isMatchingRank());
    }

    public boolean canSurrender() {
        return (this.cards.size() == 2 && this.getHandValue() < BlackJack.BLACK_JACK);
    }

    public ActionEnum getState() {
        return state;
    }

    public void setState(ActionEnum state) {
        this.state = state;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        cards.stream().forEach(card -> {
            sb.append(card);
            sb.append(" ");
        });
        sb.append("Bet: ($");
        sb.append(this.bet);
        sb.append(")");
        return sb.toString();
    }

    private boolean isMatchingRank() {
        if (this.cards.size() == 2) {
            return (cards.get(0).getRank() == cards.get(1).getRank());
        }
        return false;
    }
}
