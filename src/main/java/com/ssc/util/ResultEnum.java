package com.ssc.util;

public enum ResultEnum {
    WON, LOST, TIED, SURRENDERED;
}
