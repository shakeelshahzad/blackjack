package com.ssc.util;

public enum DecisionEnum {
    HIT("Hit"),
    STAND("Stand"),
    DOUBLE_DOWN("Double Down"),
    SPLIT("Split"),
    SURRENDER("Surrender");

    public String display;

    DecisionEnum(String display) {
        this.display = display;
    }

    public static DecisionEnum getDecision(int value) {
        switch (value) {
            case 0:
                return HIT;
            case 1:
                return STAND;
            case 2:
                return DOUBLE_DOWN;
            case 3:
                return SPLIT;
            case 4:
                return SURRENDER;
        }
        throw new IllegalStateException("Please make a valid decision.");
    }

}
