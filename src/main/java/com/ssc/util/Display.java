package com.ssc.util;

import com.ssc.entity.Card;
import com.ssc.entity.Hand;
import com.ssc.entity.Player;
import com.ssc.game.BlackJack;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;

public final class Display {

    private static final Logger logger = LogManager.getLogger(Display.class);

    public static void showCards(BlackJack game) {
        for (Player player : game.getPlayers()) {
            StringBuilder sb = new StringBuilder();
            if (player.isDealer()) {
                int score = 0;
                List<Card> cards = player.getHands().get(0).getCards();
                sb.append(String.format("%s has [", player.getName()));
                for (int i = 0; i < cards.size(); i++) {
                    if (i == 0) {
                        sb.append("(DOWN_FACED)");
                    } else {
                        score += cards.get(i).getRank().getFaceValue();
                        sb.append(cards.get(i));
                    }
                }
                sb.append(String.format("] with score of %s", score));
            } else {
                for (Hand hand : player.getHands()) {
                    sb.append(String.format("%s (Balance: $%d) with cards %s with score of %d\n", player.getName(), player.getBalance(), hand.getCards(), hand.getHandValue()));
                }
            }
            logger.debug(sb);
        }
    }

    public static void showActions(List<DecisionEnum> availableActions, Hand hand, Player player) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("[%s] ", player.getName()));
        for (DecisionEnum d : availableActions) {
            sb.append(String.format("(%d) %s ", d.ordinal() + 1, d.display));
        }
        sb.append(String.format("for [%s]", hand));
        logger.debug(sb);
    }

    public static void show(StringBuilder sb) {
        logger.debug(sb);
    }

    public static void startRound() {
        logger.debug("");
        logger.debug("**********STARTING NEW ROUND**********");
        logger.debug("");
    }

    public static void lostGame() {
        logger.debug("**********YOU HAVE LOST**********");
    }

    public static void wantsToPlay(Player player) throws IOException {
        logger.debug("Do you want to play {}? Press any key to continue and 'Q' or 'q' to exit.", player.getName());
    }

    public static void endGame() {
        logger.debug("**********THANK YOU FOR PLAYING!!!**********");
    }
}
