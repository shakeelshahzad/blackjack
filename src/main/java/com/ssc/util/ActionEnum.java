package com.ssc.util;

public enum ActionEnum {
    NONE, STAND, BUST, BLACKJACK, SURRENDER, SPLIT;
}
