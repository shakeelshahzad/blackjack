package com.ssc.exceptions;

public class GameAlreadyStartedException extends Exception {

    public GameAlreadyStartedException(String message) {
        super(message);
    }

}
