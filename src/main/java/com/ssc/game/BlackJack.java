package com.ssc.game;

import com.ssc.entity.*;
import com.ssc.exceptions.GameAlreadyStartedException;
import com.ssc.util.ActionEnum;
import com.ssc.util.ResultEnum;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class BlackJack {

    public static int MIN_BET = 10;
    public static int BLACK_JACK = 21;

    public static boolean isGameStarted = false;
    private final List<Player> PLAYERS;
    private int numberOfDecks;
    private Deck DECK;

    /**
     * <p>Creates a new {@link BlackJack} instance</p>
     *
     * <p>
     * The new instance of {@link BlackJack} adds <code>Dealer</code> along with given <code>players</code> and
     * initiates the {@link BlackJack} with shuffled {@link com.ssc.entity.Card}s
     * </p>
     *
     * @param startingBalance startingBalance for all players including dealer
     * @param numberOfDecks   how many decks e.g. 1 = 52 cards, 2 = 52 * 2 etc
     * @param players         participating in the game.
     */
    public BlackJack(int startingBalance, int numberOfDecks, List<Player> players) {
        // Add Dealer into the game
        players.add(new Dealer("Dealer", this));
        setPlayersInitialBalance(players, startingBalance);

        this.PLAYERS = players;
        this.numberOfDecks = numberOfDecks;
    }

    /**
     * <p>Starts a new BlackJack Game</p>
     *
     * <p>
     * When this method is invoked it initiates the game, place initial bets of {@value #MIN_BET}
     * and {@link BlackJack#distributeFirstHand()} cards to all players (including dealer and dealer will be served last)
     * </p>
     *
     * @throws GameAlreadyStartedException if someone tries to invoke and game is already started.
     */
    public void start() throws GameAlreadyStartedException {
        this.DECK = generateDeck(numberOfDecks);
        this.DECK.shuffle();
        if (isGameStarted) {
            throw new GameAlreadyStartedException("Game is already started!");
        }
        placeInitialBets();
        distributeFirstHand();
        anyBlackJack();
        isGameStarted = true;
    }

    /**
     * <p>Has the game finished?</p>
     *
     * <p>If no players are left on the table (except DEALER), game is considered to be over.</p>
     *
     * @return true|false
     */
    public boolean isConcluded() {
        Iterator<Player> playerIterator = this.PLAYERS.iterator();
        while (playerIterator.hasNext()) {
            Player player = playerIterator.next();
            if (!player.isDealer()) {
                if (player.getBalance() < BlackJack.MIN_BET) {
                    playerIterator.remove();
                }
            }
        }

        if (this.PLAYERS.size() == 1) {
            isGameStarted = false;
            return true;
        }
        return false;
    }

    public List<Player> getPlayers() {
        return this.PLAYERS;
    }

    public Card getNextCard() {
        return this.DECK.getNextCard();
    }

    /**
     * <p>Given a hand, give the player holding this hand.</p>
     *
     * @param hand
     * @return
     */
    public Player getPlayerFor(Hand hand) {
        for (Player player : this.PLAYERS) {
            for (Hand h : player.getHands()) {
                if (h.equals(hand)) {
                    return player;
                }
            }
        }
        return null;
    }

    public StringBuilder calculateResults() {
        Dealer dealer = getDealer();
        Hand dealerHand = dealer.getHands().get(0);

        StringBuilder sb = new StringBuilder();

        for (Player player : this.PLAYERS) {
            List<Hand> hands = player.getHands();
            for (Hand hand : hands) {
                if (!player.isDealer()) {
                    String result = finalizeResults(hand, dealerHand, player, dealer);
                    sb.append(result);
                    sb.append(String.format("\n%s's balance now is $%d", player.getName(), player.getBalance()));
                    sb.append("\n");
                }
            }
            player.emptyHands();
        }
        dealer.emptyHands();
        isGameStarted = false;
        return sb;
    }

    // PRIVATE BELOW THIS

    private String prepareStatement(Player player, ResultEnum state, Hand hand, Dealer dealer, Hand dealerHand) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%s has %s the hand %s (Score: %s) against %s for hand %s (Score: %s)", player.getName(),
                state, hand, hand.getHandValue(), dealer.getName(), dealerHand, dealerHand.getHandValue()));
        return sb.toString();
    }

    private String finalizeResults(Hand hand, Hand dealerHand, Player player, Dealer dealer) {
        ResultEnum state = null;
        switch (hand.getState()) {
            case STAND:
                if (dealerHand.getState() == ActionEnum.BUST) {
                    player.setBalance(player.getBalance() + (hand.getBetValue() + hand.getBetValue()));
                    state = ResultEnum.WON;
                    break;
                }
                if (hand.getHandValue() > dealerHand.getHandValue()) {
                    player.setBalance(player.getBalance() + (hand.getBetValue() + hand.getBetValue()));
                    state = ResultEnum.WON;
                    break;
                } else if (hand.getHandValue() < dealerHand.getHandValue()) {
                    state = ResultEnum.LOST;
                    break;
                } else {
                    if (dealerHand.getState() == ActionEnum.BLACKJACK) {
                        if (hand.getState() == ActionEnum.BLACKJACK) {
                            player.setBalance(player.getBalance() + (hand.getBetValue()));
                            state = ResultEnum.TIED;
                        } else {
                            state = ResultEnum.LOST;
                        }
                        break;
                    }
                    if (hand.getState() == ActionEnum.BLACKJACK) {
                        player.setBalance(player.getBalance() + (hand.getBetValue() + hand.getBetValue()));
                        state = ResultEnum.WON;
                    } else {
                        state = ResultEnum.TIED;
                    }
                    break;
                }
            case BUST:
                state = ResultEnum.LOST;
                break;
            case SURRENDER:
                player.setBalance(player.getBalance() + (hand.getBetValue() / 2));
                state = ResultEnum.SURRENDERED;
                break;
        }
        return prepareStatement(player, state, hand, dealer, dealerHand);
    }

    private void anyBlackJack() {
        for (Player player : this.PLAYERS) {
            for (Hand hand : player.getHands()) {
                if (hand.getHandValue() == BlackJack.BLACK_JACK) {
                    hand.setState(ActionEnum.BLACKJACK);
                }
            }
        }
    }

    private Dealer getDealer() {
        for (Player player : this.PLAYERS) {
            if (player.isDealer()) {
                return ((Dealer) player);
            }
        }
        return null;
    }

    private void placeInitialBets() {
        for (Player player : this.PLAYERS) {
            Hand hand = player.createFirstHand();
            if (!player.isDealer()) {
                ((Human) player).placeBet(MIN_BET, hand);
            }
        }
    }

    private void distributeFirstHand() {
        for (int i = 1; i <= 2; i++) {
            for (Player player : this.PLAYERS) {
                Card card = this.DECK.getNextCard();

                player.addCard(card, player.getHands().get(0));
            }
        }
    }

    private void setPlayersInitialBalance(List<Player> players, int balance) {
        players.forEach(p -> {
            if (!p.isDealer()) {
                p.setBalance(balance);
            }
        });
    }

    private Deck generateDeck(int numberOfDecks) {
        // Generate Cards
        List<Card> cards = generateCards();
        for (int i = 1; i < numberOfDecks; i++) {
            cards.addAll(cards);
        }
        return new Deck(cards);
    }

    private List<Card> generateCards() {
        List<Card> cards = new LinkedList<>();
        for (Card.Suite suite : Card.Suite.values()) {
            for (Card.Rank rank : Card.Rank.values()) {
                cards.add(new Card(suite, rank));
            }
        }
        return cards;
    }
}
