package com.ssc;

import com.ssc.action.Decision;
import com.ssc.action.DecisionFactory;
import com.ssc.action.Split;
import com.ssc.entity.Hand;
import com.ssc.entity.Human;
import com.ssc.entity.Player;
import com.ssc.exceptions.GameAlreadyStartedException;
import com.ssc.game.BlackJack;
import com.ssc.util.ActionEnum;
import com.ssc.util.DecisionEnum;
import com.ssc.util.Display;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class GamePlay {
    public static void main(String[] args) throws GameAlreadyStartedException, IOException {
        Player player1 = new Human("Shakeel");
        // Player player2 = new Human("Adeel");
        List<Player> players = new ArrayList<>();
        players.add(player1);
        //players.add(player2);

        BlackJack blackJack = new BlackJack(100, 1, players);
        DecisionFactory decisionFactory = DecisionFactory.getInstance(blackJack);

        while (!blackJack.isConcluded()) {
            Display.startRound();
            blackJack.start();

            List<Player> gamePlayers = blackJack.getPlayers();
            for (int i = 0; i < gamePlayers.size(); i++) {
                Player player = gamePlayers.get(i);
                ListIterator<Hand> handsIterator = player.getHands().listIterator();
                while (handsIterator.hasNext()) {
                    Hand hand = handsIterator.next();
                    while (hand.getState() == ActionEnum.NONE && hand.getState() != ActionEnum.BLACKJACK
                            && hand.getState() != ActionEnum.SURRENDER && hand.getState() != ActionEnum.SPLIT) {
                        List<DecisionEnum> availableActions = player.getAvailableActions(hand);
                        if (!player.isDealer()) {
                            Display.showCards(blackJack);
                            Display.showActions(availableActions, hand, player);
                        }
                        Decision decision = player.getDecision(hand, availableActions, decisionFactory);
                        decision.execute(hand, handsIterator);
                    }
                }
            }

            Display.show(blackJack.calculateResults());
            if (blackJack.isConcluded()) {
                Display.lostGame();
            } else {
                Iterator<Player> playerIterator = blackJack.getPlayers().iterator();
                while (playerIterator.hasNext()) {
                    Player player = playerIterator.next();
                    if (!player.isDealer()) {
                        Display.wantsToPlay(player);
                        boolean wantsToPlay = ((Human) player).wantsToPlay();
                        if (!wantsToPlay) {
                            playerIterator.remove();
                        }
                    }
                }
            }
        }
        Display.endGame();
    }
}
