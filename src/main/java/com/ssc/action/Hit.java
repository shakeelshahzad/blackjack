package com.ssc.action;

import com.ssc.entity.Hand;
import com.ssc.game.BlackJack;
import com.ssc.util.ActionEnum;

import java.util.ListIterator;

public class Hit extends Decision {

    public Hit(BlackJack game) {
        super(game);
    }

    @Override
    public void execute(Hand hand, ListIterator<Hand> iterator) {
        hand.addCard(this.game.getNextCard());
        int handValue = hand.getHandValue();
        if (handValue > BlackJack.BLACK_JACK) {
            hand.setState(ActionEnum.BUST);
        } else if (handValue == BlackJack.BLACK_JACK) {
            hand.setState(ActionEnum.STAND);
        } else {
            hand.setState(ActionEnum.NONE);
        }
    }
}
