package com.ssc.action;

import com.ssc.entity.Hand;
import com.ssc.game.BlackJack;
import com.ssc.util.ActionEnum;

import java.util.ListIterator;

public class Surrender extends Decision {

    public Surrender(BlackJack game) {
        super(game);
    }

    @Override
    public void execute(Hand hand, ListIterator<Hand> iterator) {
        hand.setState(ActionEnum.SURRENDER);
    }
}
