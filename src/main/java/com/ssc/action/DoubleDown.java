package com.ssc.action;

import com.ssc.entity.Hand;
import com.ssc.entity.Human;
import com.ssc.entity.Player;
import com.ssc.game.BlackJack;
import com.ssc.util.ActionEnum;

import java.util.ListIterator;

public class DoubleDown extends Decision {

    public DoubleDown(BlackJack game) {
        super(game);
    }

    @Override
    public void execute(Hand hand, ListIterator<Hand> iterator) {
        Player player = this.game.getPlayerFor(hand);
        if (!player.isDealer()) {
            ((Human) player).placeBet(BlackJack.MIN_BET, hand);
        }
        hand.addCard(this.game.getNextCard());
        int handValue = hand.getHandValue();
        if (handValue > BlackJack.BLACK_JACK) {
            hand.setState(ActionEnum.BUST);
        } else if (handValue == BlackJack.BLACK_JACK) {
            hand.setState(ActionEnum.STAND);
        } else {
            hand.setState(ActionEnum.NONE);
        }
    }
}
