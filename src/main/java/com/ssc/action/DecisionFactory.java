package com.ssc.action;

import com.ssc.game.BlackJack;
import com.ssc.util.DecisionEnum;

public final class DecisionFactory {

    private static Decision HIT;
    private static Decision DOUBLE_DOWN;
    private static Decision STAND;
    private static Decision SURRENDER;
    private static Decision SPLIT;

    private static DecisionFactory decisionFactory;

    private DecisionFactory(BlackJack game) {
        HIT = new Hit(game);
        DOUBLE_DOWN = new DoubleDown(game);
        STAND = new Stand(game);
        SURRENDER = new Surrender(game);
        SPLIT = new Split(game);
    }

    public static DecisionFactory getInstance(BlackJack game) {
        if (decisionFactory == null) {
            decisionFactory = new DecisionFactory(game);
        }
        return decisionFactory;
    }

    public Decision make(DecisionEnum choice) {
        switch (choice) {
            case HIT:
                return HIT;
            case DOUBLE_DOWN:
                return DOUBLE_DOWN;
            case STAND:
                return STAND;
            case SURRENDER:
                return SURRENDER;
            case SPLIT:
                return SPLIT;
            default:
                throw new IllegalStateException("No such action found.");
        }
    }
}
