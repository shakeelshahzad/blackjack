package com.ssc.action;

import com.ssc.entity.Card;
import com.ssc.entity.Hand;
import com.ssc.entity.Human;
import com.ssc.entity.Player;
import com.ssc.game.BlackJack;
import com.ssc.util.ActionEnum;

import java.util.List;
import java.util.ListIterator;

public class Split extends Decision {

    public Split(BlackJack game) {
        super(game);
    }

    @Override
    public void execute(Hand hand, ListIterator<Hand> iterator) {
        Hand hand1 = new Hand();
        List<Card> cards = hand.getCards();

        Card card1 = new Card(cards.get(0).getSuite(), cards.get(0).getRank());
        hand1.addCard(card1);
        hand1.addCard(this.game.getNextCard());

        Hand hand2 = new Hand();
        Card card2 = new Card(cards.get(1).getSuite(), cards.get(1).getRank());
        hand2.addCard(card2);
        hand2.addCard(this.game.getNextCard());

        Player player = this.game.getPlayerFor(hand);

        player.setBalance(player.getBalance() + hand.getBetValue());
        hand.setState(ActionEnum.SPLIT);

        iterator.remove();
        iterator.add(hand1);
        iterator.add(hand2);
        iterator.previous();
        iterator.previous();

        ((Human) player).placeBet(BlackJack.MIN_BET, hand1);
        ((Human) player).placeBet(BlackJack.MIN_BET, hand2);
    }
}
