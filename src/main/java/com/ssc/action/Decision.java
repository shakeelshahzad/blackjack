package com.ssc.action;

import com.ssc.entity.Hand;
import com.ssc.game.BlackJack;

import java.util.ListIterator;

public abstract class Decision {

    // TODO: Decision has game? Game should have decision, no?
    protected BlackJack game;

    public Decision(BlackJack game) {
        this.game = game;
    }

    public abstract void execute(Hand hand, ListIterator<Hand> iterator);
}
